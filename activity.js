db.fruits.aggregate(
	[
		{ $unwind: "$origin" },
		{ $group: { _id: "$origin", fruits: { $sum: 1 }} }
	]
)

db.fruits.aggregate([

    { $match: { onSale: true } },
    
    { $count: "fruits_on_sale" }
])

db.fruits.aggregate(



[



    { $match: { stock: {$gte: 20 }  } },

    { $count: "fruits_with_20+_Stocks"}

]



)


db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } },
]);


db.fruits.aggregate([
    { $group: { _id: "$supplier_id", 
      maxPrice: { $max: "$price" } 
    } }        
	])

db.fruits.aggregate([
    { $group: { _id: "$supplier_id", 
      minPrice: { $min: "$price" } 
    } }        
	])
